CXX = g++
CXXFLAGS = -std=c++11 -Wall -g -O2

all: test

test: main.o item.o player.o room.o container.o
	$(CXX) $(CXXFLAGS) -o test main.o item.o player.o room.o container.o

main.o: main.cpp item.h player.h room.h container.h
	$(CXX) $(CXXFLAGS) -c main.cpp

item.o: item.cpp item.h
	$(CXX) $(CXXFLAGS) -c item.cpp

player.o: player.cpp player.h
	$(CXX) $(CXXFLAGS) -c player.cpp

room.o: room.cpp room.h
	$(CXX) $(CXXFLAGS) -c room.cpp

container.o: container.cpp container.h
	$(CXX) $(CXXFLAGS) -c container.cpp

.PHONY: clean
clean:
	rm -f *.o test

