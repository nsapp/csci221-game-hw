#ifndef ROOM_H
#define ROOM_H

#include <map>
#include <string>
#include "container.h"

class Player;

class Room : public Container {
	public:
		Room();
		Room(std::string, std::string);
		std::string getTitle() const;
		std::string getDescription() const;
		Room* getAdjacent(std::string);
		void setAdjacent(std::string, Room*);
		void putItem(Item* item);
		void removeItem(Item* item);
	private:
		Player* player;
		std::map<std::string, Room*> exits;
		std::string title;
		std::string desc;
};

#endif
