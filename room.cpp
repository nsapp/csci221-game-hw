#include "room.h"

Room::Room() {
	title = "";
	desc= "";
}

Room::Room(std::string t, std::string d) {
	title = t;
	desc = d; 
}

std::string Room::getTitle() const {
	return title;
}

std::string Room::getDescription() const {
	return desc;
}

Room* Room::getAdjacent(std::string s) {
	if (exits.find(s) != exits.end()) {
		return exits[s];
	} else {
		return NULL;
	}
	return exits[s];
}

void Room::setAdjacent(std::string s, Room* r) {
	exits[s] = r;
}

void Room::putItem(Item* item) {
	items.insert(item);
}

void Room::removeItem(Item* item) {
	items.erase(item);
}

