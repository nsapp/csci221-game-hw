#include "item.h"

Item::Item(std::string str) {
	name = str;
}

Item::Item(const Item& other) {
	name = other.name;
}

Container* Item::getContainer() {
	return container;
}

std::string Item::getName() {
	return name;
}

