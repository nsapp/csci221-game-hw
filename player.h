#ifndef PLAYER_H
#define PLAYER_H

#include <set>
#include <vector>
#include "container.h"

class Room;
class Item;

class Player : public Container {
	public:
		Player(Room*);
		Room* getRoom();
		bool handleInput();

	private:
		std::string findItemPlayer(std::vector<std::string>);
		std::string findItemRoom(std::vector<std::string>);
		void setRoom(Room*);
		void pickUpItem(Item*);
		void dropItem(Item*);
		Room* position;
};

#endif
