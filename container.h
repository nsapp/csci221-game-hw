#ifndef CONTAINER_H
#define CONTAINER_H

#include <string>
#include <set>

class Item;

class Container {
	public:
		~Container();
		std::set<Item*> getItems();
		Item* findItemInSet(std::string);
		void listItems();
		
	protected:
		std::set<Item*> items;
};

#endif
