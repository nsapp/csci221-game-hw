#include <iostream>
#include "container.h"
#include "room.h"
#include "player.h"
#include "item.h"
#include <string>

void linkRooms(Room*, Room*, std::string, std::string);

int main () {
	// Create and link rooms
	Room* cell = new Room("Cell", "A dank, musty cell. You don't know why you're here or how you got here, only that you are."
					"The north door to your cell is wide open, and there's a book on the ground. Convenient.");
	Item* book = new Item("book");
	cell->putItem(book);
	Room* hallway = new Room("Hallway", "A cobblestone hallway lined with cells. No living beings reside in them, "
					"only corpses in differing stages of decay. To the north, you see a ladder leading to a conveniently open trapdoor.");
	linkRooms(cell, hallway, "north", "south");
	Room* dungeon = new Room("Dungeon", "Just your average castle dungeon. Linining the walls of the moderately sized room "
					"are cells, all of which are empty. To the west is a gate leading to yet another hallway, which is locked. "
					"The trapdoor leading down to "
					"the cell in which you found yourself is open. In stroke of luck, you see a key on the ground which appears "
					"to fit the door.");
	linkRooms(hallway, dungeon, "north", "south");
	Item* key = new Item("key");
	dungeon->putItem(key);
	Room* greatHall = new Room("Great Hall", "The great hall of the castle. At one point, it was well kept and inviting; now, "
					"it has clearly suffered from lack of use, as evidenced by the cobwebs which seem to cover everything. "
					"To the south is a large door which appears to lead out of the castle. On the east wall is a corridor leading "
					"to the dungeon. A staircase can be seen on the western wall, leading up to a second story.");
	linkRooms(dungeon, greatHall, "west", "east");
	Room* landing = new Room("Landing", "The stairs on the western wall of this room lead down to the Great Hall. A doorway to the "
					"south reveals an armory, while the north archway leads to an owlery");
	linkRooms(greatHall, landing, "west", "west");
	Room* armory = new Room("Armory", "Formerly used to house weapons and armor for the castle guards and others, the armory now "
					"contains only a sword which, conveniently, is the correct size for you. The northern doorway leads back to the landing.");
	Item* sword = new Item("sword");
	armory->putItem(sword);
	linkRooms(landing, armory, "south", "north");
	Room* owlery = new Room("Owlery", "The owlery: a place in which owls roosted, presumably for message sending/receiving. There is a note on"
					" the ground. The southern door leads to the landing, while a large window on the western wall would allow you to climb onto "
					"the roof of the castle.");
	Item* note = new Item("note");
	owlery->putItem(note);
	linkRooms(landing, owlery, "north", "south");
	Room* roof = new Room("Roof", "It's the roof. It's pretty high, even though it's only a two-story castle. Nowhere to go but back east from "
					"whence you came. There is a feather on the roof.");
	Item* feather = new Item("feather");
	roof->putItem(feather);
	linkRooms(owlery, roof, "west", "east");
	Room* courtyard = new Room("Courtyard", "A large open area outside the castle, but still inside the walls. North are the doors to enter the "
					"great hall, while to the south is the castle gate. Conveniently, it is open.");
	linkRooms(greatHall, courtyard, "south", "north");
	Room* freedom = new Room("Outside", "Freedom!");
	linkRooms(courtyard, freedom, "south", "north");

	// Create player in starting room 
	Player* p = new Player(cell);

	// Starting text
	std::cout << "Welcome to CREATIVE_GAME_TITLE! Typing 'help' will bring up the Help Menu." << std::endl;

	// Game loop
	while (p->handleInput());

	// Memory management
	delete cell;
	delete hallway;
	delete dungeon;
	delete greatHall;
	delete landing;
	delete armory;
	delete owlery;
	delete roof;
	delete courtyard;
	delete freedom;
	delete p;
	return 0;
}

void linkRooms(Room* r1, Room* r2, std::string link1, std::string link2) {
	r1->setAdjacent(link1, r2);
	r2->setAdjacent(link2, r1);
}

