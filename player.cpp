#include "player.h"
#include "room.h"
#include "item.h"
#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <vector>
#include <iostream>

std::string findVerb(std::vector<std::string>);
std::string findDirection(std::vector<std::string>);
std::string findItem(std::vector<std::string>);
void printHelp();

Player::Player(Room* room) {
	position = room;
}

Room* Player::getRoom() {
	return position;
}

// I'm not proud of this code but it runs without leaking memory so...

bool Player::handleInput() {
	std::cout << "> ";
	std::string input;
	getline(std::cin, input);

	if (input == "help") {
		printHelp();
		return true;
	} else if (input == "quit") {
		return false;
	} else if (input == "look") {
		std::cout << this->position->getTitle() << std::endl;
		std::cout << this->position->getDescription() << std::endl;
		return true;
	} else if (input == "invent") {
		this->listItems();
	}

	// move words to a vector
	std::istringstream iss(input);
	std::vector<std::string> parsed((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

	// Get useful words out of the vector
	std::string verb = findVerb(parsed); 
	std::string noun;
	if (verb == "go") {
		noun = findDirection(parsed);	
		// Make sure there exists an exit in that direction
		if (this->position->getAdjacent(noun) != NULL) {
			this->position = this->position->getAdjacent(noun);
		} else {
			std::cout << "No way to go " << noun << " in " << this->position->getTitle() << "." << std::endl;
		}
	} else if (verb == "take") {
		noun = this->findItemRoom(parsed);
		if (noun != "") {
			this->pickUpItem(this->position->findItemInSet(noun));
		}
	} else if (verb == "drop") {
		noun = this->findItemPlayer(parsed);
		if (noun != "") {
			this->dropItem(findItemInSet(noun));
		}
	}
	return true;
}

void Player::setRoom(Room* room) {
	position = room;
}

void Player::pickUpItem(Item* item) {
	Item* other = new Item(*item);
	items.insert(other);
	this->position->removeItem(item);
	delete item;
}

void Player::dropItem(Item* item) {
	Item* other = new Item(*item);
	this->position->putItem(other);
	items.erase(item);
	delete item;
}

std::string findVerb(std::vector<std::string> strs) {
	for (int i = 0; i < (int)strs.size(); i++) {
		std::transform(strs[i].begin(), strs[i].end(), strs[i].begin(), ::tolower);	
		if (strs[i] == "go" || strs[i] == "take" || strs[i] == "drop") {
			return strs[i];
		}
	}
	return "";
}

std::string findDirection(std::vector<std::string> strs) {
	for (int i = 0; i < (int)strs.size(); i++) {
		std::transform(strs[i].begin(), strs[i].end(), strs[i].begin(), ::tolower);	
		if (strs[i] == "north" || strs[i] == "south" || strs[i] == "east" || strs[i] == "west") {
			return strs[i];
		}
	}
	return "";
}

std::string Player::findItemPlayer(std::vector<std::string> strs) {
	for (int i = 0; i < strs.size(); i++) {
		if (this->findItemInSet(strs[i]) != NULL) {
			return strs[i];
		}
	}
	return "";
}

std::string Player::findItemRoom(std::vector<std::string> strs) {
	for (int i = 0; i < strs.size(); i++) {
		if (this->position->findItemInSet(strs[i]) != NULL) {
			return strs[i];
		}
	}
	return "";
}

void printHelp() {
	std::cout << "Help menu:" << std::endl;
	std::cout << "'help' - Open the help menu" << std::endl;
	std::cout << "'quit' - Quit the game" << std::endl;
	std::cout << "'look' - Print description of the room you are currently in" << std::endl;
	std::cout << "'go <direction>' - Move in the specified direction, if available" << std::endl;
	std::cout << "'take <item>' - Pick up the specified item, if it is available" << std::endl;
	std::cout << "'drop <item>' - Drop the specified item" << std::endl;
	std::cout << "'invent' - List the items in the player's inventory" << std::endl;
}

