#include <iostream>
#include "container.h"
#include "item.h"

Container::~Container() {
	for (auto it = items.begin(); it != items.end(); it++) {
		delete *it;
	}
}

std::set<Item*> Container::getItems() {
	return items;
}

Item* Container::findItemInSet(std::string str) {
	for (std::set<Item*>::iterator it = items.begin(); it != items.end(); it++) {
		if ((*it)->getName() == str) {
			return *it; 
		}
	}
	return NULL;
}

void Container::listItems() {
	for (auto it = items.begin(); it != items.end(); it++) {
		std::cout << (*it)->getName() << " ";
	}
	std::cout << std::endl;
}

