#ifndef ITEM_H
#define ITEM_H

#include <string>

class Container;
class Room;

class Item {
	public:
		Item(std::string);
		Item(const Item&);
		Container* getContainer();
		std::string getName();	

	private:
		Container* container;
		std::string name;
};

#endif
